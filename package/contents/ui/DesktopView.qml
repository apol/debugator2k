/*
    SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleix.pol_gonzalez@mbition.io>

    SPDX-License-Identifier: BSD-3-Clause
*/

import QtQuick
import org.kde.kwin as KWinComponents

Rectangle {
    id: desktopView
    color: "gray"

    required property QtObject desktop

    readonly property QtObject targetScreen: KWinComponents.SceneView.screen
    readonly property real horizontalScale: width / targetScreen.geometry.width
    readonly property real verticalScale: height / targetScreen.geometry.height

    property QtObject currentWindow: null

    Repeater {
        model: KWinComponents.WindowFilterModel {
            activity: KWinComponents.Workspace.currentActivity
            desktop: desktopView.desktop
            screenName: targetScreen.name
            windowModel: KWinComponents.WindowModel {}
        }

        delegate: KWinComponents.WindowThumbnail {
            wId: model.window.internalId
            x: (model.window.x - targetScreen.geometry.x) * desktopView.horizontalScale
            y: (model.window.y - targetScreen.geometry.y) * desktopView.verticalScale
            z: model.window.stackingOrder
            width: model.window.width * desktopView.horizontalScale
            height: model.window.height * desktopView.verticalScale
            visible: !model.window.minimized
            opacity: !desktopView.currentWindow || desktopView.currentWindow === model.window ? 1 : 0.3
            scale: thumbnailArea.containsMouse ? 1.1 : 1

            Behavior on scale {
                SmoothedAnimation {
                    velocity: 1
                }
            }
            Behavior on opacity {
                SmoothedAnimation {
                    velocity: 1
                }
            }

            MouseArea {
                id: thumbnailArea
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if (desktopView.currentWindow === model.window) {
                        desktopView.currentWindow = null
                    } else {
                        desktopView.currentWindow = model.window
                    }
                }
            }
        }
    }
}
