/*
    SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleix.pol_gonzalez@mbition.io>

    SPDX-License-Identifier: BSD-3-Clause
*/

import QtQuick
import QtQuick.Layouts
import QtQuick.Window
import QtQuick.Controls as QQC
import org.kde.plasma.components as PlasmaComponents3
import org.kde.kwin as KWinComponents

Rectangle {
    id: root

    focus: true
    color: "white"

    property var currentScreenGeometry: undefined

    Component {
        id: selectedWindowComponent
        PlasmaComponents3.ScrollView {
            contentHeight: ccc.height

            contentData: ListView {
                id: ccc
                width: parent.width
                header: QQC.Button {
                    text: "Back"
                    onClicked: vvv.currentWindow = null
                }
                model: vvv.currentWindow ? Object.keys(vvv.currentWindow).filter(name => !name.endsWith("Changed")).sort() : undefined
                delegate: PlasmaComponents3.Label {
                    Layout.fillWidth: true
                    text: "- " + modelData + " = " + vvv.currentWindow[modelData]
                }
            }
        }
    }

    Component {
        id: generalDebugComponent
        PlasmaComponents3.ScrollView {
            contentHeight: ccc.height

            contentData: ListView {
                id: ccc
                width: parent.width
                model: KWinComponents.WindowFilterModel {
                    //screenName: KWinComponents.SceneView.screen.name //Show all screens
                    activity: KWinComponents.Workspace.currentActivity
                    desktop: KWinComponents.Workspace.currentDesktop
                    windowModel: KWinComponents.WindowModel {}
                }
                delegate: PlasmaComponents3.ItemDelegate {
                    Layout.fillWidth: true
                    text: "- " + model.window.caption + ": " + model.window
                    checkable: true
                    opacity: !root.currentScreenGeometry || root.currentScreenGeometry === model.window.output.geometry ? 1 : 0.3
                    onClicked: vvv.currentWindow = model.window
                }
            }
        }
    }

    Loader {
        id: debugContents
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
        width: parent.width / 3
        sourceComponent: vvv.currentWindow ? selectedWindowComponent : generalDebugComponent
    }

    ListView {
        anchors {
            top: parent.top
            right: parent.right
            left: debugContents.right
            bottom: vvv.top
        }

        model: Qt.application.screens
        delegate: PlasmaComponents3.ItemDelegate {
            text: modelData.name + ": " + modelData.width + "x" + modelData.height + " @ " + modelData.virtualX + ", " + modelData.virtualY
            onClicked: {
                const geo = Qt.rect(modelData.virtualX, modelData.virtualY, modelData.width, modelData.height);
                if (root.currentScreenGeometry === geo) {
                    root.currentScreenGeometry = null
                } else {
                    root.currentScreenGeometry = geo
                }
            }
        }
    }

    DesktopView {
        id: vvv
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: root.width - debugContents.width - 2 * 10
        height: width * (root.height / root.width)
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        desktop: KWinComponents.Workspace.currentDesktop
    }
}
