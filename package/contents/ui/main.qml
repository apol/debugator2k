/*
    SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleix.pol_gonzalez@mbition.io>

    SPDX-License-Identifier: BSD-3-Clause
*/

import QtQuick
import org.kde.kirigami as Kirigami
import QtQuick.Controls as QQC
import org.kde.kwin as KWinComponents

KWinComponents.SceneEffect {
    id: effect

    property bool activated: false
    readonly property int animationDuration: Kirigami.Units.longDuration

    delegate: ScreenView {
        QQC.Button {
            icon.name: "dialog-close"
            anchors.top: parent.top
            anchors.right: parent.right
            onClicked: effect.deactivate()
        }
        Keys.onEscapePressed: deactivate()
    }

    KWinComponents.ScreenEdgeHandler {
        mode: KWinComponents.ScreenEdgeHandler.Pointer
        edge: KWinComponents.ScreenEdgeHandler.LeftEdge
        onActivated: toggle();
    }

    KWinComponents.ScreenEdgeHandler {
        mode: KWinComponents.ScreenEdgeHandler.Touch
        edge: KWinComponents.ScreenEdgeHandler.LeftEdge
        onActivated: toggle();
    }

    KWinComponents.ScreenEdgeHandler {
        mode: KWinComponents.ScreenEdgeHandler.Pointer
        edge: KWinComponents.ScreenEdgeHandler.TopEdge
        onActivated: toggle();
    }

    KWinComponents.ScreenEdgeHandler {
        mode: KWinComponents.ScreenEdgeHandler.Touch
        edge: KWinComponents.ScreenEdgeHandler.TopEdge
        onActivated: toggle();
    }


    KWinComponents.ShortcutHandler {
        name: "Debuggator 2K"
        text: "Toggle Debuggator 2K"
        sequence: "Meta+5"
        onActivated: toggle();
    }

    Timer {
        id: deactivateTimer
        interval: effect.animationDuration
        onTriggered: effect.visible = false
    }

    function toggle() {
        if (activated) {
            deactivate();
        } else {
            activate();
        }
    }

    function activate() {
        if (activated || deactivateTimer.running) {
            return;
        }
        visible = true;
        activated = true;
    }

    function deactivate() {
        if (!activated) {
            return;
        }
        activated = false;
        deactivateTimer.start();
    }
}
